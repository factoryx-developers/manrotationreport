<?php
/**
generate a item rotation report
- stock in listed stores
- product disabled online

select style, sum(qtyoh) from style_soh where style like 'g%' and qtyoh > 0 and LOCATION in ('x03') group by STYLE

-v verbose
-s stores
-d delete temporary sql & csv files
-e send emails
-t test, send to admin only

# crontab
php -f manRotationReport.php -- --mode=prod --store=x03 -v --stores=x03

## cavets
RDS prevents access to the FILE permission for all users

hack
mysql -h db -u username -p \
    --batch -e "select * from sales_flat_order" \
    | sed 's/\t/","/g;s/^/"/;s/$/"/;s/\n//g' > /tmp/file.csv

sed 's/\t/","/g;s/^/"/;s/$/"/;s/\n//g'
mysql --host=fx.shared.rds.prod.int.aws.factoryx.com.au --user=claude_maus --password=XXXXX -D mage_cm_prod --batch < /tmp/20141211-032546_x07_report.sql | sed 's//","/g;s/^/"/;s/$/"/;s///g' > /tmp/20141211-032546_x07_report.csv
z
http://dba.stackexchange.com/questions/17029/cannot-output-mysql-data-to-file
*/
ini_set('display_errors',1);
error_reporting(E_ALL);

date_default_timezone_set('Australia/Melbourne');

require_once("_dbHosts.php");

require __DIR__ . '/sendgrid-php/sendgrid-php.php';

class Report {

    private $debug = false;

    // cli args
    private $cliMode;
    private $cliStore;
    private $cliVerbose = false;
    private $cliEmail = false;
    private $cliDeleteFiles = false;
    private $cliTest = false;
    private $cliStores = array();

    private $includeSeason = array(
        'W18','S18','W19'
    );

    private $storeSkuPrefix = array(
        'x03' => 'g'
    );

    private $fromDate;
    private $toDate;
    private $datesSpecified = false;

    private $output = "";

    private static $INCLUDE_HEADER = true;

    protected $adminName = "Ben Incani";
    protected $adminEmailAddress = "ben.incani@factoryx.com.au";

    public function __construct($fromDate, $options) {

        $this->cliMode = "dev";
        // prod | dev
        if (array_key_exists("mode", $options)) {
            $this->cliMode = $options["mode"];
        }
        $this->cliStore = null;
        if (array_key_exists("store", $options)) {
            $this->cliStore = $options["store"];
        }
        if (array_key_exists("v", $options)) {
            $this->cliVerbose = true;
        }
        if (array_key_exists("e", $options)) {
            $this->cliEmail = true;
        }
        if (array_key_exists("d", $options)) {
            $this->cliDeleteFiles = true;
        }
        if (array_key_exists("t", $options)) {
            $this->cliTest = true;
            $this->debug = true;
        }
        if (array_key_exists("stores", $options)) {
            $this->cliStores = explode(",", $options["stores"]);
        }

        $this->message(sprintf("options: %s\n", print_r($options, true)));
    }

    public function run() {
        $mageProductsSql = $GLOBALS["mageProductsSql"];
        $this->message(sprintf("mageProductsSql: %s", $mageProductsSql));

        $outputFiles = array();

        // now run SQL the code
        $label = sprintf("%s-%s", date('Ymd-His'), basename(__FILE__, ".php"));
        foreach($GLOBALS["mysqlDatabases"][$this->cliMode] as $storeCode => $db) {

            $this->message(sprintf("-->%s[%s]", $storeCode, $this->cliMode));

            if ($this->cliStore && strcmp($this->cliStore, $storeCode) != 0) {
                $this->message(sprintf("SKIP [%s <> %s] = %d", $storeCode, $this->cliStore, strcmp($this->cliStore, $storeCode)) );
                continue;
            }

            $pathToCsv = sprintf("/tmp/%s_%s_report.csv", $label, $storeCode);
            if (sys_get_temp_dir()) {
                $pathToCsv = sprintf("%s/%s_%s_report.csv", sys_get_temp_dir(), $label, $storeCode);
            }
            $this->message(sprintf("out '%s'", $pathToCsv));


            $host = $GLOBALS["fxReporting"]['host'];
            $username = $GLOBALS["fxReporting"]['username'];
            $password = $GLOBALS["fxReporting"]['password'];
            $database = $GLOBALS["fxReporting"]['database'];
            if ($this->debug) {
                echo sprintf("%s->host: %s\n", __METHOD__, $host);
                echo sprintf("%s->username: %s\n", __METHOD__, $username);
                echo sprintf("%s->password: %s\n", __METHOD__, $password);
                echo sprintf("%s->database: %s\n", __METHOD__, $database);
            }
            // manual error suppression
            $dbhFxReporting = new mysqli($host, $username, $password, $database);
            if ($dbhFxReporting->connect_errno) {
                $err= sprintf("Failed to connect to MySQL: (%d) %s", $dbhFxReporting->connect_errno,  $dbhFxReporting->connect_error);
                //trigger_error($err, E_USER_ERROR);
                throw new Exception($err);
            }
            $groupBy = "";
            if ($byStyle = false) {
                $groupBy = "style";
                /*
                get style stock from stores
                */
                $sql = sprintf("select s.style, cat, sum(qtyoh) as 'total_qtyoh' from style_soh ss left join style s on s.style = ss.style where s.style like '%s%%' and qtyoh > 0 and cat in ('%s')",
                    $this->storeSkuPrefix[$storeCode],
                    implode("','", $this->includeSeason)
                );
                $mageProductsSql = preg_replace("/%TYPE_ID%/", "configurable", $mageProductsSql);
            }
            else {
                $groupBy = "item";
                /*
                get item stock from stores
                */
                $sql = sprintf("select i.item, i.cat, sum(qtyoh) as 'total_qtyoh' from item_soh isoh left join item i on i.item = isoh.item where i.item like '%s%%' and qtyoh > 0 and i.cat in ('%s')",
                    $this->storeSkuPrefix[$storeCode],
                    implode("','", $this->includeSeason)
                );
                $mageProductsSql = preg_replace("/%TYPE_ID%/", "simple", $mageProductsSql);
            }
            if ($this->cliStores) {
                $sql .= sprintf(" and LOCATION in ('%s')", implode("','", $this->cliStores));
            }
            // group by
            $sql .= sprintf(" group by %s", $groupBy);

            $this->message(sprintf("sql: %s\n", $sql));
            $stockOnHand = $dbhFxReporting->query($sql);

            $accpacProducts = array();
            foreach($stockOnHand as $stock) {
                //echo sprintf("result: %s\n", print_r($stock, true));
                $accpacProducts[$stock[$groupBy]] = $stock['total_qtyoh'];
            }

            /*
            get magento products and check if its disabled
            */
            $dbhMage = $this->_getDatabase($storeCode, $configOnly = false);
            $this->message(sprintf("sql: %s\n", $mageProductsSql));

            if ($mageProducts = $dbhMage->query($mageProductsSql)) {
                $header = array("consol","qty");
                array_unshift($header, $groupBy);
                $csvFh = fopen($pathToCsv,"w");
                fputcsv($csvFh, $header);

                //$this->message(sprintf("products: %d|%s\n", count($mageProducts), print_r($mageProducts, true)));
                while ($mageProduct = $mageProducts->fetch_assoc()) {
                    if ($mageProduct['status'] == 'disabled') {
                        //$this->message(sprintf("result: %s\n", print_r($product, true)) );
                        $key = strtoupper($mageProduct['sku']);
                        if ($groupBy == "style") {
                            $key = strtoupper(substr($mageProduct['sku'], 0, 10));
                        }
                        if (array_key_exists($key, $accpacProducts)) {
                            $data = array($key, $mageProduct['consolidated'], $accpacProducts[$key]);
                            //$this->message(sprintf("%s\n", print($data, true)) );
                            fputcsv($csvFh, $data);
                        }
                        else {
                            $data = array($key, $mageProduct['consolidated'], "not-in-accpac");
                            //$this->message(sprintf("%s\n", print($data, true)) );
                            //fputcsv($csvFh, $data);
                        }
                    }
                }

                if ($this->cliEmail) {
                    $recipients = $GLOBALS["mysqlDatabases"][$this->cliMode][$storeCode]['recipients'];
                    $this->sendReportEmail($pathToCsv, $storeCode, $recipients);
                }
        
                // delete report
                if ($this->cliDeleteFiles) {
                    $this->message(sprintf("delete '%s' %s", $pathToCsv, (unlink($pathToCsv)? "OK" : "FAILED!" )) );
                }
            }
        }

    }

    /**
    $storeCode
    $this->cliMode
    */
    private function _getDatabase($storeCode, $configOnly = false) {
        if (!array_key_exists($this->cliMode, $GLOBALS["mysqlDatabases"])) {
            throw new Exception(sprintf("cannot find env '%s'", $this->cliMode));
        }
        //echo sprintf("check: %s\n", $storeCode);
        if (!array_key_exists($storeCode, $GLOBALS["mysqlDatabases"][$this->cliMode])) {
            throw new Exception(sprintf("cannot find store code '%s->%s'", $this->cliMode, $storeCode));
        }
        if ($configOnly) {
            return $GLOBALS["mysqlDatabases"][$this->cliMode][$storeCode];
        }
        else {
            $host = $GLOBALS["mysqlDatabases"][$this->cliMode][$storeCode]['host'];
            $username = $GLOBALS["mysqlDatabases"][$this->cliMode][$storeCode]['username'];
            $password = $GLOBALS["mysqlDatabases"][$this->cliMode][$storeCode]['password'];
            $database = $GLOBALS["mysqlDatabases"][$this->cliMode][$storeCode]['database'];
            if ($this->debug) {
                echo sprintf("%s->host: %s\n", __METHOD__, $host);
                echo sprintf("%s->username: %s\n", __METHOD__, $username);
                echo sprintf("%s->password: %s\n", __METHOD__, $password);
                echo sprintf("%s->database: %s\n", __METHOD__, $database);
            }
            // manual error suppression
            @$mysqli = new mysqli($host, $username, $password, $database);
            if ($mysqli->connect_errno) {
                $err= sprintf("Failed to connect to MySQL: (%d) %s", $mysqli->connect_errno,  $mysqli->connect_error);
                //trigger_error($err, E_USER_ERROR);
                throw new Exception($err);
            }
            return $mysqli;
        }
    }

    /**
     * Function name
     *
     * what the function does
     *
     * @param (type) (name) about this param
     * @return (type) (name)
     */
    protected function sendReportEmail($file, $storeCode, $recipients) {
        $subject = sprintf("%s Warehouse Rotation Report %s",
            $storeCode,
            date("Y-m-d")
        );
        $message = sprintf("details:\n");
        $message .= sprintf(" desc: %s\n", $subject);
        $message .= sprintf(" date: %s\n", date('c'));
        $message .= sprintf(" host: %s\n", gethostname());
        $message .= sprintf(" file: %s\n", __FILE__);
        $message .= sprintf(" data: %s\n", $file);

        $message .= sprintf("\n--- DEBUG ---\n%s", $this->output);

        $files = array();
        $files[] = $file;
        foreach($recipients as $recipient) {
            $this->message(sprintf("email report to %s", $recipient));
            $this->sendEmailAttachment($recipient, $subject, $message, $htmlEmail = false, $files);
        }
    }

    /**
     * sendEmailAttachmentV2
     * uses sendgrid lib
     *
     * @param string $recipient
     * @param string $subject
     * @param string $message
     * @param bool $htmlEmail
     * @param array $files
     */
    function sendEmailAttachment($recipient, $subject, $message, $htmlEmail = false, $files) {
        $sendgrid = new \SendGrid($GLOBALS["sendgridApiKey"]);

        $recipientName = $recipient;
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($this->adminEmailAddress, $this->adminName);
        $email->setReplyTo($this->adminEmailAddress, $this->adminName);
        $email->setSubject($subject);
        $email->addTo($recipient, $recipientName);
        $mimeType = "text/plain";
        if ($htmlEmail) {
            $mimeType = "text/html";
        }
        $email->addContent("text/plain", $message);

        foreach ($files as $file) {
           if (!is_file($file) || filesize($file) == 0) {
                $this->message(sprintf("file[%s]: %d|%d", $file, is_file($file), filesize($file)) );
                continue;
            }
            $name = basename($file);
            $file_size = filesize($file);
            $handle = fopen($file, "r");
            $content = fread($handle, $file_size);
            fclose($handle);
            //$content = chunk_split(base64_encode($content));
            $content = base64_encode($content);

            $attachment = new \SendGrid\Mail\Attachment();
            $attachment->setContent($content);
            $attachment->setType($mimeType = "text/csv");
            $attachment->setFilename($name);
            $attachment->setDisposition("attachment");
            $attachment->setContentId("report");

            //$this->message(sprintf("filename: %s", $name));
            //$this->message(sprintf("addAttachment: %s", get_class($attachment)));
            $email->addAttachment($attachment);
        }
        $retVal = false;
        try {
            $response = $sendgrid->send($email);
            $retVal = $response->statusCode();
            // 202 ACCEPTED - the request has been accepted for processing
            $this->message(sprintf("statusCode: %s", $response->statusCode()) );
            $this->message(sprintf("headers: %s", print_r($response->headers(), true)) );
            $this->message(sprintf("body: %s", $response->body()) );
        }
        catch(Exception $ex) {
            $retVal = false;
        }
        return $retVal;
    }

    protected function message($msg) {
        if ($this->cliVerbose) {
            echo sprintf("%s\n", $msg);
        }
    }
}

/*
SQL heredoc

-- old method of unsold sku's
cpe.sku NOT IN (select sku from tmp_sold_skus)
*/

// sku, name, status, date uploaded, age [days], sold
$mageProductsSql = <<<SQL
select
    cpe.sku,
    cpe.type_id,
    cpev_nam.value,
    if (cpei_sta.value = 1, 'enabled','disabled') as 'status',
    if (cpei_con.value = 1, 'yes','no') as 'consolidated',
    DATE_FORMAT(cpe.created_at,'%d/%c/%Y') as 'date uploaded',
    DATEDIFF(CURDATE(),cpe.created_at) as 'age [days]'
from catalog_product_entity cpe
left join catalog_product_entity_int cpei_con on cpei_con.entity_id = cpe.entity_id AND cpei_con.attribute_id = (select attribute_id from eav_attribute where attribute_code = 'online_only')
left join catalog_product_entity_int cpei_sta on cpei_sta.entity_id = cpe.entity_id AND cpei_sta.attribute_id = (select attribute_id from eav_attribute where attribute_code = 'status' AND source_model = 'catalog/product_status')
left join catalog_product_entity_varchar cpev_nam on cpev_nam.entity_id = cpe.entity_id AND cpev_nam.attribute_id = (select attribute_id from eav_attribute where attribute_code = 'name' AND entity_type_id IN (4, 10))
where cpe.type_id = '%TYPE_ID%'
group by cpe.sku, cpev_nam.value, cpei_sta.value, cpei_con.value, cpe.created_at;
SQL;

// Script example.php
$shortopts  = "";
//$shortopts .= "f:";  // Required value
//$shortopts .= "v::"; // verbose
//$shortopts .= "e::"; // email
$shortopts .= "vedt"; // These options do not accept values

$longopts  = array(
    "mode:",     // mode (required)
    "store::",   // store (optional)
    "stores::",  // stores (optional)
    "from::",    // from date (optional)
    "to::",      // to date (optional)
//  "opt",       // opt (switch)
);

$options = getopt($shortopts, $longopts);

if (array_key_exists("v", $options)) {
    echo sprintf("options: %s\n", var_export($options, true));
}

$report = new Report("2013-01-01", $options);
$report->run();

?>
